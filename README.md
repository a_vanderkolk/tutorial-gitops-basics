# GitOps Basics tutorial
- [GitOps Basics tutorial](#gitops-basics-tutorial)
  - [Goal](#goal)
  - [Tools](#tools)
    - [Tekton](#tekton)
    - [ArgoCD](#argocd)
    - [Helm](#helm)
    - [MicroK8s](#microk8s)
    - [Gitea](#gitea)
  - [Prerequisites](#prerequisites)
  - [Sources](#sources)

In the past I've used Jenkins to build and deploy my applications. This time I am trying something new and try to follow the GitOps philosophy. In this tutorial I will share my findings. I hope you can use these knowledge in your advantage. 

In the basics tutorial we will go through all the steps necessary to build a application and deploy it on the cluster. 

## Goal
When you've have finished the tutorial, you have a Pipeline to build a Java Application and deploy it to your cluster. In this tutorial I will go over the basics on how things work. 

The Workflow will look something like this:

```
+--------------------+                      +--------------------+                   
|                    |                      |                    |                   
|   Clone Source     |           +----------|   Clone Infra      |                   
|   Code Repository  |           |          |   Repository       |                   
|                    |           |          |                    |                   
+--------------------+           |          +--------------------+                   
           |                     |                     |                             
           |                     |                     |                             
           |                     |                     |                             
+--------------------+           |          +--------------------+                   
|                    |           |          |                    |                   
|   Build Java       |           |          |   Create branch    |                   
|   Application      |           |          |                    |                   
|                    |           |          |                    |                   
+--------------------+           |          +--------------------+                   
           |                     |                     |                             
           |                     |                     |                             
           |                     |                     |                             
+--------------------+           |          +--------------------+                   
|                    |           |          |                    |                   
|   Build Image      |           |          |  Update            |                   
|                    |           |          |  Infrastructure    |                   
|                    |           |          |                    |                   
+--------------------+           |          +--------------------+                   
           |                     |                     |                             
           |                     |                     |                             
           |                     |                     |                             
+--------------------+           |          +--------------------+                   
|                    |           |          |                    |                   
|  Collect           |           |          |  Commit changes    |                   
|  Information       |-----------+          |                    |                   
|                    |                      |                    |                   
+--------------------+                      +--------------------+                   
                                                       |                             
                                                       |                             
                                                       |                             
                                            +--------------------+                   
                                            |                    |                   
                                            |  Create Pull       |                   
                                            |  Request           |                   
                                            |                    |                   
                                            +--------------------+                   
```

In Chapter 1 I will go into the details of GitOps. 


## Tools
For this tutorial I've used the following Tools:

### Tekton
I've used Tekton for Continuous Integration pipelines. A lot of this tutorial will be about the usage of Tekton

### ArgoCD
ArgoCD will be used for the Continuous Deployment part of the application. 

### Helm
I like using Helm as my k8s package manager. It makes deploying much easier. I won't go into advance Helm tricks in this tutorial. I am planning to make a advanced tutorial where I will Helm everything. This 

### MicroK8s
I am using MicroK8s as my Single Node Cluster solution. I am running microk8s on a NUC, you can run it on your own PC if you wish. 

For documentation about installing MicroK8s see: https://ubuntu.com/tutorials/install-a-local-kubernetes-with-microk8s#1-overview 

The configuration of MicroK8s is out of the scope for this tutorial. I've used the following add-ons for MicroK8s:
* dns
* dashboard
* storage
* ingress
* rbac
* registry
* storage

You can also use [Minikube](https://minikube.sigs.k8s.io/docs/start/) or [Code Ready Containers](https://access.redhat.com/documentation/en-us/red_hat_codeready_containers/1.0/html/getting_started_guide/getting-started-with-codeready-containers_gsg) if you like. Please take into account that we need RBAC and Storage for this tutorial. 

### Gitea
I needed a Git Repository with the ability to create Pull (or Merge) Requests. GitLab was to heavy on my NAS, took too much resources on my NUC, so here we are. 

## Prerequisites
Before you start with this tutorial, I assume you have the following installed/ready

* A Gitea instance:
    * [With Docker](https://docs.gitea.io/en-us/install-with-docker/)
    * [On a NAS](https://thomes.blog/2020/12/09/how-to-gitea-on-synology/)
    * [With the Binart](https://docs.gitea.io/en-us/install-from-binary/)
* [Helm](https://helm.sh/docs/intro/install/)
* [MicroK8s](https://microk8s.io/)
* Git Client


## Sources
I didn't come up with this all by myself. I've had some great help from fellow Tekton/GitOps enthusiasts. Please find the sources I've used below

* https://redhat-scholars.github.io/tekton-tutorial/tekton-tutorial/index.html
* https://helm.sh/docs/
* https://about.gitlab.com/topics/gitops/