# 2. Introduction Tekton
- [2. Introduction Tekton](#2-introduction-tekton)
  - [2.1. What is Tekton?](#21-what-is-tekton)
    - [2.1.1. Tasks](#211-tasks)
    - [2.1.2. Pipelines](#212-pipelines)
    - [2.1.3. Workspaces](#213-workspaces)
    - [2.1.4. Triggers](#214-triggers)
  - [2.2. Why (I) use Tekton](#22-why-i-use-tekton)

In this tutorial we will use Tekton as our main tool for the Continuos Integration part. 

## 2.1. What is Tekton?
> Tekton is a powerful and flexible open-source framework for creating CI/CD systems, allowing developers to build, test, and deploy across cloud providers and on-premise systems.
>
> https://tekton.dev/

### 2.1.1. Tasks
Tekton basically is a framework that allows you to perform `tasks` on your kubernetes platform. These `tasks` are build with `steps`. Each `step` contains a script or a command with arguments. These commands / scripts will be ran inside a `container`. Each `step` needs a `image` to start the `container`.

When the `task` is finished, the `container` will stop and no longer take your resources. 

### 2.1.2. Pipelines
You can use `pipelines` to chain `Tasks` together. You can even use the results from `tasks` as an argument for a new `task`. The `pipeline` allows you to set an order of `tasks` to run. 

### 2.1.3. Workspaces
Pipelines allow you to use `workspaces`. Workspaces are used to share objects between tasks. You can use a Persistent Volume Claim (PVC) to pull the repository to. In the next step you can use this PVC to perform a Maven Build on. 

### 2.1.4. Triggers
Running `pipelines` manually is fun and all, but the real magic of `GitOps` is to automate as much as possible. To start a `pipeline` automatically, you can use `Triggers`. A trigger contains a `EventListener` and a mapping between data from the event to parameters of the `pipeline`. 

I use Pull Requests as events for my Pipelines. With a EventListener you can also filter these events. So for example, only listen for closed PRs. 


## 2.2. Why (I) use Tekton
For years I've used Jenkins to build and deploy my applications. Don't get me wrong, there is nothing wrong with Jenkins. Jenkins is highly customizable and extendable with pretty much everything you need. So in my opinion you can use Jenkins to achieve `GitOps` as well. 

The thing that bothered me with Jenkins is the overhead. You need to have at least one Jenkins running. This costs resources (which are limited on my NUC). I also needed `Agents` to run specific tasks for me. These `agents` were a bit of a pain. 

With the 4.x releases of OpenShift I got introduced with Tekton. Tekton is allowing me to start tasks and finish them on the go. This only uses my resources when it is actually doing something. When the task is finished, the resources are not longer needed. There is a bit of a disclaimer; when using EventListeners, we need to have a Pod that is constantly running.

Also creating these tasks are a lot easier. If I want to run a specific action with `Bash` I can just provide a `Bash` script. If I need to do something more complex, I can use a `Python` script. I can just manage the script within the task without changing something in for example a global library. 

