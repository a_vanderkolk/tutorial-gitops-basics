# 3. Installing Tekton
- [3. Installing Tekton](#3-installing-tekton)
  - [3.1. Prerequisites](#31-prerequisites)
  - [3.2. Install Tekton](#32-install-tekton)
  - [3.3. Check the installation](#33-check-the-installation)
  - [3.4. Install the dashboard](#34-install-the-dashboard)
  - [Install the Command Line Client](#install-the-command-line-client)

This chapter will describe how to install Tekton on your cluster. 

## 3.1. Prerequisites
I assume you have the following in order:
- A running instance of Kubernetes (For example MicroK8s)
- Kubernetes allows use to use `RBAC`
- The `.kube` config is correctly configured to use the correct cluster
- You have Admin privileges on the Cluster 

## 3.2. Install Tekton
Luckily, installing Tekton is quite simple. Just apply a `yaml` file from Google.

```
kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
```

## 3.3. Check the installation
First check if the pods are running correctly:

```
kubectl get pods --namespace tekton-pipelines
```

Also check the volumes:

```
kubectl get pv
kubectl get storageclasses
```

## 3.4. Install the dashboard
You can do everything you need to do with the `tkn` CLI tool. But sometimes you wish to have a visual representation of what happened. The dashboard is not required (OpenShift has its own build in representation of the pipelines) but it could come in handy. 

To install the dashboard:

```
kubectl apply --filename https://github.com/tektoncd/dashboard/releases/latest/download/tekton-dashboard-release.yaml
```

By default, the dashboard is not exposed using Ingress. So you can use port forwarding to expose the service:

```
kubectl --namespace tekton-pipelines port-forward svc/tekton-dashboard 9097:9097
```

You could also create a `Ingress` definition to always expose your Tekton Dashboard on a address. Please note you need to have your internal network DNS to use these addresses. This is outside the scope of this tutorial. 

You can find an example yaml [here](../chapter_03-Installing_Tekton/files/ingress.yaml)

If you are using OpenShift you need to use a `Route`. 

## Install the Command Line Client
You need to install the `tkn` CLI tool. This tool is used to run Tekton commands on the cluster. Follow the instruction on this page: https://tekton.dev/docs/getting-started/

You can check if the client is correctly installed with:

```
tkn version
```