# 1. Introduction GitOps
- [1. Introduction GitOps](#1-introduction-gitops)
  - [1.1. What is GitOps?](#11-what-is-gitops)
    - [1.1.1. Infrastructure as Code (IaC)](#111-infrastructure-as-code-iac)
    - [1.1.2. Pull / Merge Requests (PRs / MRs)](#112-pull--merge-requests-prs--mrs)
    - [1.1.3. CI/CD](#113-cicd)
  - [1.2. Why use GitOps?](#12-why-use-gitops)

This chapter will give a short introduction into GitOps. 

## 1.1. What is GitOps?
A definition:
> GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.
> 
> [GitLab](https://about.gitlab.com/topics/gitops/)

GitOps combines best practice from the DevOps framework and combines it with the Git Workflow (versioning control, reviews, collaboration, etc.). It applies to the infrastructure automation. 

GitOps requires three components:

> GitOps = IaC + PRs (or MRs) + CI/CD

### 1.1.1. Infrastructure as Code (IaC)
Infrastructure as Code means that you are treating your Infrastructure definition (for Kubernetes objects) as code. With other words, you are versioning it with Git. The main goal of IaC is to have one definition that is the same for each environment. So when applying the Infrastucture, the result should be the same everywhere. 

Because you are defining your Infrastructure definitions as Code, you are applying your basic Git Workflow to it. For example, let your peers review it before using it to 'production'. 

### 1.1.2. Pull / Merge Requests (PRs / MRs)
As briefly mentioned in the previous chapter, with GitOps you are using Pull Requests to allow reviews on your changes before applying it to your production environment. 

### 1.1.3. CI/CD
Continuous Integration / Continuous Delivery should be clear for every developer in this day and age. Basically you are using tools and chained processes to build and deliver your applications. In these steps you make sure the delivered software is of a high standard.

## 1.2. Why use GitOps?
When building Cloud Native applications, you want that each release of your software is the same. You want to prevent manual operations that are sensitive for human errors. The bases of each release should be the same. So for example, when using a DTAP (Development, Testing, Acceptance, Production) you want the behavior and the installation of your application to be the same. 

When defining your infrastucture in Git, you can use this infrastructure for each release on each environment. This is done by a automated process that prevents the risk of human errors. Using reviews on the IaC makes sure that the release is checked by developers (or opsers) before deployment. 
