# 4. Our first tasks
- [4. Our first tasks](#4-our-first-tasks)
  - [4.1. Introduction](#41-introduction)
  - [4.2. Create a Helm Chart](#42-create-a-helm-chart)
  - [4.3. Create the CICD namespace](#43-create-the-cicd-namespace)
  - [4.4. Create the 'Hello World' Task](#44-create-the-hello-world-task)
  - [4.5. Using Script](#45-using-script)
  - [4.6. Using parameters](#46-using-parameters)
  - [4.7. TaskRuns](#47-taskruns)
  - [4.8. Using other languages](#48-using-other-languages)
  - [4.9. ClusterTasks](#49-clustertasks)
  - [4.10. Pro tip: Task hub](#410-pro-tip-task-hub)
  - [4.11. Cleaning](#411-cleaning)

In this chapter we will create our first task.

Please see the following page for the original documentation:
https://tekton.dev/docs/pipelines/tasks/

## 4.1. Introduction
As mentioned before, Tekton Tasks are containing one or more steps. Each step will perform a specific job and then stop. The main goal of each step is to perform a action for delivering your software. 

The tasks we will create in this chapter will be quite useless. The goal is however to get you familiar with Tekton Tasks. 

## 4.2. Create a Helm Chart
As mentioned, we will use Helm to deploy the Kubernetes Resources on the cluster. Using Helm allows us to deploy multiple resources at once. It also allows us to use variables in a later stage. In this tutorial we will only use some basic Helm functions. 

Initialize our Continuos Integration Chart:

```
helm create gitops-ci
```

Delete everything we don't need for now:

```bash
rm gitops-ci/templates/*.yaml
rm gitops-ci/templates/tests -Rf
rm gitops-ci/charts -Rf
echo "Installed Tekton CI pipeline with name {{ .Release.Name }} with version {{ .Chart.Version }} successfully in namespace {{ .Release.Namespace }}" > gitops-ci/templates/NOTES.txt
```

## 4.3. Create the CICD namespace
First we will create the cicd namespace:

```
kubectl create namespace cicd
kubectl config set-context --current --namespace cicd
```

We are now using the cicd namespace

## 4.4. Create the 'Hello World' Task
As any self respecting developer knows, you can not call yourself a developer without printing 'Hello World' somewhere. So this is exactly what we are going to do. 

Open the newly created Helm Chart folder with your favorite IDE. I like to use [Visual Studio Code](https://code.visualstudio.com/) for Helm Charts. I installed the following plugins:
- [Kubernetes](https://marketplace.visualstudio.com/items?itemName=ms-kubernetes-tools.vscode-kubernetes-tools)
- [Helm Intellisense](https://marketplace.visualstudio.com/items?itemName=Tim-Koehler.helm-intellisense)

Because we are going to create a lot of resources in our Helm Chart, we will create a subfolder for our tasks. So create the folder `templates/tasks`

Now create a new yaml file `templates/tasks/hello-world-1.yaml`

Add the following code in this file:

```yaml
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: hello-world-1
spec:
  description: |
    This task just echoes "Hello World" to demo the usage of command with args.
  steps:
    - name: hello-world
      image: alpine
      command: 
      - echo
      args:
      - "Hello world!"
```

| Key                     | Description                                                                                           |
| :---------------------- | :---------------------------------------------------------------------------------------------------- |
| `metadata.name`         | The name of the Task                                                                                  |
| `spec.description`      | This allows you to describe the goal of the task                                                      |
| `spec.steps`            | This is an array. The order of the steps is sequential                                                |
| `spec.steps[n].name`    | A descriptive name of the action to take                                                              |
| `spec.steps[n].image`   | This is the image dat is being used to run the task. The commands used must be available in the image |
| `spec.steps[n].command` | Also an array. These are the tasks that are executed when the container is started                    |
| `spec.steps[n].args`    | Also an array. These are the arguments that are used when running the command                         |



We can now install our task using Helm:

```bash
cd gitops-ci
helm upgrade --install gitops-ci ./
```

We can now test our Task with the `tkn` cli tool.

```bash
tkn task start hello-world-1 --showlog
```

The output looks something like this:

```
TaskRun started: hello-world-1-run-xtbfb
Waiting for logs to be available...
[hello-world] Hello world!
```

## 4.5. Using Script
You can also use the script key to run a command. This allows us to run multiple commands without creating a step for each command. 

Working with scripts is my preferred method. 

Let's create a `Task` with script (doing the exact same thing as the previous task).

Create a new yaml file `templates/tasks/hello-world-2.yaml`. Add the following code:

```yaml
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: hello-world-2
spec:
  description: |
    This task just echoes "Hello World" to demo the usage of script
  steps:
    - name: hello-world
      image: alpine
      script: |
        echo "Hello World!"
```

Upgrade the Helm Chart with the command:

```bash
helm upgrade --install gitops-ci ./
```

Now we can run our new task:

```
tkn task start hello-world-2 --showlog
```

The output looks like this:

```
TaskRun started: hello-world-2-run-xfn76
Waiting for logs to be available...
[hello-world] + echo 'Hello World!'
[hello-world] Hello World!
```

## 4.6. Using parameters
Because not every command is exactly the same, we can use parameters. These parameters can be used within the `steps`. In the example below we are going to use parameters to make the task more dynamic. 

We are going to modify `templates/tasks/hello-world-2.yaml` to take parameters.

```yaml
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: hello-world-2
spec:
  description: |
    This task just echoes "Hello World" to demo the usage of script
  params:
    - name: image
      description: The Image that is used to run the commands
      default: alpine
    - name: subject
      description: The subject to say Hello to
      default: World
  steps:
    - name: hello-world
      image: $(params.image)
      script: |
        echo "Hello $(params.subject)!"
```

As you can see, we've added two parameters:
- image
- subject


| Key | Description |
| :-- | :-- |
| `spec.params[n].name` | The name of the parameter. This name will be used to get the value of the parameter |
| `spec.params[n].description` | Optional; Used for documentation purposes of your task. |
| `spec.params[n].default` | Optional; Used to provide a default value for a parameter. |

You can call the value of the parameter like this:

```
$(params.subject)
```

If we run the Task without any values, it will ask us for the values:

```
tkn task start hello-world-2 --showlog
```

Output:

```
? Value for param `image` of type `string`? (Default is `alpine`) alpine
? Value for param `subject` of type `string`? (Default is `World`) World
TaskRun started: hello-world-2-run-jfjrc
Waiting for logs to be available...
[hello-world] + echo 'Hello World!'
[hello-world] Hello World!
```

You can prevent this by adding `--use-param-defaults`.

Of course you can also provide the values for the parameters:

```
tkn task start hello-world-2 \
--param subject=John \
--use-param-defaults \
--showlog
```

Now the output looks like this:

```
TaskRun started: hello-world-2-run-l8hm5
Waiting for logs to be available...
[hello-world] + echo 'Hello John!'
[hello-world] Hello John!
```

## 4.7. TaskRuns
In the previous paragraphs we have started the tasks using the `tkn task start` command. Each time you run this command, a `TaskRun` object will be created. We can list these objects with the command:

```
tkn taskrun ls
# or 
tkn tr ls 
```

Output could look like this:

```
NAME                      STARTED         DURATION    STATUS
hello-world-2-run-h2wsk   1 minute ago    6 seconds   Succeeded
hello-world-2-run-t7f2n   1 minute ago    7 seconds   Succeeded
hello-world-2-run-r879p   1 minute ago    6 seconds   Succeeded
hello-world-1-run-47gqn   2 minutes ago   6 seconds   Succeeded
```

We can check the logs for a specific run:

```
tkn tr logs hello-world-1-run-47gqn
```

We can also get more information for a specific task run:

```
tkn tr describe hello-world-1-run-47gqn
```

Output:
```
Name:              hello-world-1-run-47gqn
Namespace:         cicd
Task Ref:          hello-world-1
Service Account:   default
Timeout:           1h0m0s
Labels:
 app.kubernetes.io/managed-by=Helm
 tekton.dev/task=hello-world-1

🌡️  Status

STARTED          DURATION    STATUS
11 minutes ago   6 seconds   Succeeded

📨 Input Resources

 No input resources

📡 Output Resources

 No output resources

⚓ Params

 No params

📝 Results

 No results

📂 Workspaces

 No workspaces

🦶 Steps

 NAME            STATUS
 ∙ hello-world   Completed

🚗 Sidecars

No sidecars
```

It is also possible to `cancel` a specific task (for example when it is stuck). 


## 4.8. Using other languages
In the previous paragraphs, we have used `bash` to run our task. It is also possible to run a task with a different scripting language. 

Create a new yaml file `templates/tasks/hello-world-python.yaml`. 

We can now add the following content:

```yaml
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: hello-world-python
spec:
  description: |
    This task just echoes "Hello World" to demo the usage of script
  params:
    - name: image
      description: The Image that is used to run the commands
      default: python
    - name: subject
      description: The subject to say Hello to
      default: World
  steps:
    - name: hello-world
      image: $(params.image)
      script: |
        #!/usr/bin/env python3
        def greet(name):
          print("Hello {} from Python!".format(name))
        
        greet("$(params.subject)")

```

As you can see we are now using the `python` image. It is important a image that supports your script.

Note the `#!/usr/bin/env python3` at the first line. 

We still can use `$(params.subject)` in our script to map the parameters to our script. 


## 4.9. ClusterTasks
In the previous paragraphs we have used the resource type `Task`. This makes the `Task` available within the Namespace. You can change this type to `ClusterTask`. A `ClusterTask` is available from all the Namespaces on the cluster. 

Create a new yaml file `templates/tasks/hello-world-ct.yaml`. Add the following content:

```yaml
apiVersion: tekton.dev/v1beta1
kind: ClusterTask
metadata:
  name: hello-world-ct
spec:
  description: |
    This task just echoes "Hello World" to demo the usage of command with args.
  steps:
    - name: hello-world
      image: alpine
      command: 
      - echo
      args:
      - "Hello world!"
```

Install the Helm Chart

```bash
helm upgrade --install gitops-ci ./
```

Now create a new namespace and switch to this namespace:

```
kubectl create namespace clustertask-demo
kubectl config set-context --current --namespace clustertask-demo
```

Check if you can run the `hello-world-1` task:

```
$ tkn task start hello-world-1
Error: Task name hello-world-1 does not exist in namespace clustertask-demo
```

Now check if you can run the clustertask:

```
tkn clustertask start hello-world-ct --showlog 
TaskRun started: hello-world-ct-run-t8xvs
Waiting for logs to be available...
[hello-world] Hello world!
```

Note: we are using `clustertask` instead of `task`.

## 4.10. Pro tip: Task hub
Tekton has a hub that contains a lot of tasks. Most of this tasks will come in very handy. Check this page:
https://hub.tekton.dev/


## 4.11. Cleaning
Now we can cleanup our previous stuff:

```
# delete the namespace
kubectl delete namespace clustertask-demo
# switch to cicd namespace again
kubectl config set-context --current --namespace clustertask-demo
# Delete the task runs
tkn tr delete --all -f
```

