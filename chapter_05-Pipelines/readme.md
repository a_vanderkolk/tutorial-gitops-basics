# 1. Fake 1
# 2. Fake 2
# 3. Fake 3
# 4. Fake 4
# 5. Using Pipelines 
- [1. Fake 1](#1-fake-1)
- [2. Fake 2](#2-fake-2)
- [3. Fake 3](#3-fake-3)
- [4. Fake 4](#4-fake-4)
- [5. Using Pipelines](#5-using-pipelines)
  - [5.1. Clone the Quarkus App repository](#51-clone-the-quarkus-app-repository)

Running `tasks` is fun and all, but we are interest in chaining `tasks` together with an `pipeline`. This chapter will go over the subject of `pipelines`.

## 5.1. Clone the Quarkus App repository
